# README #

The Align tool allows you to align or distribute selected tags along the axis you specify.
 
The Align plug-in for Autodesk® Revit® can help to save time while producing complex drawings with large sets of annotation.
 
Just select a few tags and the Align tool will sort them for you.

### What is this repository for? ###

* The entire source code of the Align Revit Plug-In can be found here. Feel free to use it for  whatever you want.
* VErsion 1.0.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact